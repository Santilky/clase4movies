//
//  SearchForTermResponse.swift
//  MoviesCatalog
//
//  Created by Santiago Linietsky on 05/02/2022.
//

import Foundation

struct SearchForTermResponse: Codable {
    var Search: [Movie]
    struct Movie: Codable {
        var Title: String
        var Year: String
        var imdbID: String
        var Poster: String
    }
}
