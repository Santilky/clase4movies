//
//  MovieDetailResponse.swift
//  MoviesCatalog
//
//  Created by Santiago Linietsky on 10/02/2022.
//

import Foundation

struct MovieDetailResponse: Codable {
    var Title: String
    var Year: String
    var Rated: String
    var Released: String
    var Runtime: String
    var Genre: String
    var Director: String
    var Writer: String
    var Actors: String
    var imdbRating: String
}
