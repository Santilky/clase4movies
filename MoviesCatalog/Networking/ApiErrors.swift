//
//  ApiErrors.swift
//  MoviesCatalog
//
//  Created by Santiago Linietsky on 08/02/2022.
//

import Foundation

enum ApiMovieError{
    case noInternetConnection
    case notFound
    case notValidResponse
}

