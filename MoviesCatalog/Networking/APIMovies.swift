//
//  APIMovies.swift
//  MoviesCatalog
//
//  Created by Santiago Linietsky on 05/02/2022.
//

import Foundation

class ApiMovies {
    static let url = "https://movie-database-imdb-alternative.p.rapidapi.com/"
    static let token = ProcessInfo.processInfo.environment["KEY"]!
    static let host = "movie-database-imdb-alternative.p.rapidapi.com"
    // MARK: API Calls
    static func searchForTerm(term: String, onSuccess: @escaping (SearchForTermResponse)->Void, onError: @escaping (ApiMovieError?)->Void) {
        let request = generateRequestForUrl(url: "\(url)?s=\(term)&r=json&page=1")
        self.genericCall(
            request: request,
            onSuccess: onSuccess,
            onError: onError
        )
    }
    static func searchForId(id: String, onSuccess: @escaping (MovieDetailResponse)->Void, onError: @escaping (ApiMovieError?)->Void ) {
        let request = generateRequestForUrl(url: "\(url)?i=\(id)&r=json")
        self.genericCall(
            request: request,
            onSuccess: onSuccess,
            onError: onError
        )
    }
    // MARK: Generic Static Private Metods
    private static func genericCall<T: Codable>(request: URLRequest, onSuccess: @escaping (T)->Void, onError: @escaping (ApiMovieError?)->Void) {
        let session = URLSession(configuration: .default)
        session.dataTask(with: request) { data, response, error in
            print(response)
            if let data = data, let response = response {
                do {
                    let decoder = JSONDecoder()
                    let result = try decoder.decode(T.self, from: data)
                    onSuccess(result)
                } catch {
                    onError(.notFound)
                }
            }
        }.resume()
    }
    static func generateRequestForUrl(url: String) -> URLRequest{
        var request = URLRequest(url: URL(string: url)!,timeoutInterval: Double.infinity)
        request.addValue(host, forHTTPHeaderField: "x-rapidapi-host")
        request.addValue(token, forHTTPHeaderField: "x-rapidapi-key")
        request.httpMethod = "GET"
        return request
    }
}
