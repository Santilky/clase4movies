//
//  MovieCelTableViewCell.swift
//  MoviesCatalog
//
//  Created by Santiago Linietsky on 08/02/2022.
//

import UIKit
protocol MovieCellModel {
    var movieName: String {get set}
    var year: String {get set}
    var poster: UIImage? {get set}
}
protocol MovieCellDelegate: AnyObject {
    func onClick(id: Int)
}
class MovieCelTableViewCell: UITableViewCell {
    static var identifier = "MovieCelTableViewCell"
    @IBOutlet weak var MovieYear: UILabel!
    @IBOutlet weak var movietitle: UILabel!
    @IBOutlet weak var posterImage: UIImageView!
    
    @IBOutlet weak var onClickButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    weak var delegate: MovieCellDelegate?

    func setupModel(data: MovieCellModel) {
        movietitle.text = data.movieName
        MovieYear.text = data.year
        posterImage.image = data.poster
        onClickButton.removeTarget(nil, action: nil, for: .allEvents)
        onClickButton.addTarget(self, action: #selector(self.onClick), for: .touchDown)
    }
    func setTag(id: Int) {
        self.onClickButton.tag = id
    }
    @objc private func onClick() {
        delegate?.onClick(id: onClickButton.tag)
    }
    
}
