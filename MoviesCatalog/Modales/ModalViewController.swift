//
//  ModalViewController.swift
//  MoviesCatalog
//
//  Created by Santiago Linietsky on 08/02/2022.
//

import Foundation
import UIKit
protocol ModalModel {
    var buttonName: String {get set}
    var title: String {get set}
    var message: String {get set}
//    var target: Any {get set}
//    var action: Selector {get set}
}
class ModalViewController: UIViewController {
    
    lazy var titleLabel: UILabel = {
        let label: UILabel = .init()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    lazy var messageLabel: UILabel = {
        let label: UILabel = .init()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    lazy var buttonLabel: UIButton = {
        let button: UIButton = .init()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.blue, for: .normal)
        return button
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    private func setupUI() {
        self.view.backgroundColor = .init(red: 200/255, green: 130/255, blue: 130/255, alpha: 0.3)
        let newView = UIView()
        newView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(newView)
        NSLayoutConstraint.activate(
            [
                newView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                newView.leftAnchor.constraint(equalTo: view.leftAnchor),
                newView.rightAnchor.constraint(equalTo: view.rightAnchor),
                newView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5)
            ]
        )
        newView.backgroundColor = .white
        newView.addSubview(titleLabel)
        newView.addSubview(messageLabel)
        newView.addSubview(buttonLabel)
        NSLayoutConstraint.activate(
            [
                titleLabel.topAnchor.constraint(equalTo: newView.topAnchor, constant: 50),
                titleLabel.centerXAnchor.constraint(equalTo: newView.centerXAnchor),
                titleLabel.leftAnchor.constraint(equalTo: newView.leftAnchor, constant: 30),
                titleLabel.rightAnchor.constraint(equalTo: newView.rightAnchor, constant: -30),
                messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 40),
                messageLabel.centerXAnchor.constraint(equalTo: newView.centerXAnchor),
                messageLabel.leftAnchor.constraint(equalTo: newView.leftAnchor, constant: 30),
                messageLabel.rightAnchor.constraint(equalTo: newView.rightAnchor, constant: -30),
                buttonLabel.bottomAnchor.constraint(equalTo: newView.bottomAnchor, constant: -50),
                buttonLabel.centerXAnchor.constraint(equalTo: newView.centerXAnchor),
            ]
        )
    }
    func setup(data: ModalModel) {
        titleLabel.text = data.title
        messageLabel.text = data.message
        buttonLabel.setTitle(data.buttonName, for: .normal)
        buttonLabel.addTarget(self, action: #selector(self.onCloseModal), for: .touchDown)
//        buttonLabel.addTarget(data.target, action: data.action, for: .touchDown)
    }
    @objc func onCloseModal() {
        self.dismiss(animated: true)
    }
}

